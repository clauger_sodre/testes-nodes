module.exports = class Cliente{
    constructor(nome, idade, email, senha){
        this.nome = nome;
        this.idade = idade;
        this.email = email;
        this.senha = senha;
        this.dataCadastro = new Date();
    }
    funcao(){
        console.log("============função=============")
    }
    static funcaoStatica(){
        console.log("============funçãoStatica=============")
    }
}