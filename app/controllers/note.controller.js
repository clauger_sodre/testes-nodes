//Cria as funçoes para acessar o banco mongoDb

const Note = require('../models/note.model.js');

// Create and Save a new Note
exports.create = (req, res) => {
    console.log(req)
    // Validate request
    if(!req.body){
        console.log("valor: "+req.body)
        return res.status(400).send({
            message: "Nome content can not be empty"
            
        });
        
    }

    // Create a Note
    const note = new Note({
        nome: req.body.nome,
        email: req.body.email,
        cpf: req.body.cpf        
    });

    // Save Note in the database
    note.save()
        .then(data => {
            res.send(data);
            console.log("Nome: "+ data.nome+" Conteudo do CPF: "+data.cpf);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Note."
            });
        });
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Note.find()
        .then(notes => {
            res.send(notes);
        }).catch(err => {
            res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
            });
    });
};

// Find a single note with a noteId
exports.findOne = (req, res) => {
    Note.findById(req.params.clienteId)
    .then(note => {
        if(!note) {
            return res.status(404).send({
            message: "Cliente not found with id " + req.params.clienteId
        });
    }
    res.send(note);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Cliente not found with id " + req.params.clienteId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Cliente with id " + req.params.clienteId
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
    let clienteId = req.params.clienteId;
        let nome =req.body.nome;
        let email = req.body.email;
        let  cpf =req.body.cpf ;
        console.log("bodoy===================================================");
        console.log(res.data);
        console.log("===================================================");
        console.log(req.data)
        console.log("===================================================");
        console.log(res.body);
        console.log("nome: "+nome+" email: "+email+" cpf: "+cpf);
    // Validate Request
    if(!req.body) {
       
        return res.status(400).send({
            message: "Corpo da mensagem vazio"
        });
    }
    
        
    // Find note and update it with the request body
        
    Note.findByIdAndUpdate(req.params.clienteId, {
        nome: req.body.nome,
        email: req.body.email,
        cpf: req.body.cpf  
    }, {new: true})
    .then(note => {
        if(!note) {
            return res.status(404).send({
                message: "Cliente not found with id " + req.params.clienteId
            });
        }
        res.send(note);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Cliente not found with id " + req.params.clienteId
            });                
        }
        return res.status(500).send({
            message: "Error updating Cliente with id " + req.params.clienteId
        });
    });

};

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Note.findByIdAndRemove(req.params.clienteId)
    .then(note => {
        if(!note) {
            return res.status(404).send({
                message: "Cliente not found with id " + req.params.clienteId
            });
        }
        res.send({message: "Cliente deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.clienteId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.clienteId
        });
    });


};
