const express = require('express')
const router = express.Router()
const routsController = require('../controllers/note.controller.js');

//Requisita o Token	
require("dotenv").config();// As variáveis de ambientes estão em .env na raiz
const jwt = require('jsonwebtoken');
//


// Cria os endpoints GET POST PUT da aplicaçãos
   
   // define a simple route
     router.get('/app',verifyJWT, (req, res, next) => { //verifica se possui token valido antes
        res.json({"message": "Welcome to first application RestFull with Nodes."});
    });
   

    // Create a new Note
    router.post('/cliente',verifyJWT, routsController.create);

    // Retrieve all Notes
    router.get('/cliente',verifyJWT, routsController.findAll);

    // Retrieve a single Note with noteId
    router.get('/cliente/:clienteId',verifyJWT, routsController.findOne);

    // Update a Note with noteId
    router.put('/cliente/:clienteId',verifyJWT, routsController.update);  
    
    // Delete a Note with noteId
    router.delete('/cliente/:clienteId',verifyJWT, routsController.delete);

   //authentication
   router.post('/login', (req, res) => {
    //esse teste abaixo deve ser feito no seu banco de dados
   // console.log("===========================");
    //console.log("usuário: "+req.body.username," ",req.body.password)
      if(req.body.username === 'luiz' && req.body.password === '123'){
         //auth ok
         const id = 1; //esse id viria do banco de dados
         const token = jwt.sign({ id }, process.env.SECRET, {
         expiresIn: 3000 // expires in 50min basem em segundos
         });
      return res.json({ auth: true, token: token });
    }
    
    res.status(500).json({message: 'Login inválido!'});
   })

   1
2
3
// Faz logout
   router.post('/logout', function(req, res) {
      res.json({ auth: false, token: null });
   })
    module.exports = router 

//Verifica se possui Token
    function verifyJWT(req, res, next){
      //const token = req.headers['x-access-token'];
     //console.log("++++++++++++++++++++++");
      //console.log(req.headers);
      const trataToken = req.headers['authorization'];
      const token = trataToken.slice('7');//O token sempre vem com a palavra 'Bearer ' no inicio
      //console.log("token recebido: "+req.headers['x-access-token']);
      if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
      
      jwt.verify(token, process.env.SECRET, function(err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.' });
        
        // se tudo estiver ok, salva no request para uso posterior
        req.userId = decoded.id;
        next();
      });
    }