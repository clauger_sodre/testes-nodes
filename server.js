const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')

const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('./swagger_output.json')


//GitLabClone = 


// create express app
const app = express();


//Endpoit para o Swagger
app.use('/swagger-ui.html', swaggerUi.serve, swaggerUi.setup(swaggerFile))

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json())

app.use(cors()) //habilitando cors na nossa aplicacao




// Require  routes
const rotas = require('./app/routes/note.routes.js')
app.use('/', rotas)

// listen for requests
app.listen(3000, () => {
    console.log("Server is listening on port 3000");
});

// Configuring the database
const dbConfig = require('../node_restfull_express/configApp/database.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

const axios = require('axios').default;
// Pode ser algum servidor executando localmente: 
// http://localhost:3000

const api = axios.create({
    baseURL: "http://localhost:8089",
});



// Utilizando o método HTTP POST
//onst response = await api.post("posts", {image, title, content });
//const response = api.post("app/add", {
//
//    nome: "Quinto Usuario Api",
//    email: "quinto@gmail.com",
//});

//index.js
const Cliente = require("./app/models/Cliente");
const cliente1 = new Cliente("NomeQualquer", 31, "email@emailqualquer.com.br");
console.log(cliente1);
cliente1.funcao();
Cliente.funcaoStatica();
// Buscando usuários na api
api.get("/app/all").then(function (response) {
    // handle success
    var dados_recebidos =response.data;
  //  console.log("Dados recebido completo: ");
    let tamanhoData = response.data.lenth;
   // console.log("=====================");
   // console.log(tamanhoData);
   // console.log(dados_recebidos);
    //console.log("nome resgatado: "+response.data["4"].nome+" email: "+response.data["4"].email);
    //console.log(response.data);
   // console.log("Dados recebidos na posição 0: "+dados_recebidos[''].[0].['nome'].[0]);
})

    .catch(function (error) {
        // handle error
        console.log(error);
    })